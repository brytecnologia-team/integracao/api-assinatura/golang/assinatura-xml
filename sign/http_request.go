package sign

import (
	"bytes"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

//creates a new http request with content-type multipart/form-data
// params - extra parameters (key -> field name, value -> content)
// files - form files as parameters (key -> field name, value -> file path)
func NewFormDataRequest(uri string, params map[string]string, files map[string]string) (*http.Request, error) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	if files != nil {
		for key, val := range files {
			err := addFormFile(writer, key, val)
			if err != nil {
				return nil, err
			}
		}
	}
	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	err := writer.Close()
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(http.MethodPost, uri, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	return req, err
}

//add a form file as a parameter
func addFormFile(writer *multipart.Writer, paramFile, path string) error{
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	fi, err := file.Stat()
	if err != nil {
		return err
	}
	file.Close()

	part, err := writer.CreateFormFile(paramFile, fi.Name())
	if err != nil {
		return err
	}
	_, err = part.Write(fileContents)
	return err
}


