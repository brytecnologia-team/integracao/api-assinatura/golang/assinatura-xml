package sign

import (
	"assinatura-xml/config"
	"assinatura-xml/mapper"
	"assinatura-xml/model"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

func Initialize(cert model.BryCertificate) *http.Response {
	params := map[string]string {
		"nonce":  config.Nonce,
		"binaryContent": config.BinaryContent,
		"profile": config.Profile,
		"signatureFormat": config.SignatureFormat,
		"hashAlgorithm": config.HashAlgorithm,
		"certificate": cert.GetX509Base64(),
		"operationType": config.OperationType,
		"originalDocuments[0][nonce]": config.NonceOfDocument,
	}
	files := map[string]string {
		"originalDocuments[0][content]": config.DocumentPath,
	}

	req, err := NewFormDataRequest(config.UrlInitializeSignature, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != 200 {
		str, err := mapper.JsonMapper{}.DecodeToString(resp)
		if err != nil {
			log.Fatal(err)
		}
		log.Fatal(str)
	}
	return resp
}

func GetContentInitializationResponse(resp *http.Response) []model.PKCS1Dto {
	response, err := mapper.JsonMapper{}.DecodeToResponseInitialization(resp)
	if err != nil {
		log.Fatal(err)
	}
	data, err := mapper.Pkcs1Mapper{}.DecodeToPkcs1(*response)
	if err != nil {
		log.Fatal(err)
	}
	return data
}

func EncryptSignedAttributes(attributes []model.PKCS1Dto) {
	if len(attributes) > 0 {
		content, err := base64.StdEncoding.DecodeString(attributes[0].SignedAttribute)
		if err != nil {
			log.Fatal(err)
		}
		attributes[0].SignatureValue = model.LoadCertificate().Sign(content)
	}
}

func Finalize(attributes []model.PKCS1Dto){
	cert := model.LoadCertificate()
	files := map[string]string{}
	params := map[string]string{
		"nonce": config.Nonce,
		"binaryContent": config.BinaryContent,
		"profile": config.Profile,
		"signatureFormat": config.SignatureFormat,
		"hashAlgorithm": config.HashAlgorithm,
		"certificate": cert.GetX509Base64(),
		"operationType": config.OperationType,
	}

	for index, attr := range attributes {
		params[ "finalizations["+strconv.Itoa(index)+"][nonce]" ] = strconv.Itoa(attr.Nonce)
		params[ "finalizations["+strconv.Itoa(index)+"][signatureValue]" ] = attr.SignatureValue
		params[ "finalizations["+strconv.Itoa(index)+"][initializedDocument]" ] = attr.InitializedDocument
		files[ "finalizations["+strconv.Itoa(index)+"][document]" ] = attr.OriginalDocument
	}

	req, err := NewFormDataRequest(config.UrlFinalizeSignature, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	reportFinalization(resp)
}

func reportFinalization(response *http.Response){
	rf, err := mapper.JsonMapper{}.DecodeToResponseFinalization(response)
	if err != nil {
		log.Fatal(err)
	}
	if len(rf.Signatures) > 0 {
		contentDecoded, err := base64.StdEncoding.DecodeString(rf.Signatures[0].Content)
		if err != nil {
			log.Fatal(err)
		}

		f, err := os.Create("./signature.xml")
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		f.Write(contentDecoded)

		log.Println("XML signature saved: ./signature.XML")
	}
}
