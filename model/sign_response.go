package model

type Content struct {
	Nonce int
	Content string
}

type ResponseInitialization struct {
	Nonce int
	SignedAttributes []Content
	InitializedDocuments []Content
}

type ResponseFinalization struct {
	Nonce int
	Signatures []Content
}