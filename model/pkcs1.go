package model

type PKCS1Dto struct {
	Nonce int //nonce
	Content string

	SignedAttribute string //content
	SignatureValue string
	InitializedDocument string
	OriginalDocument string
}
