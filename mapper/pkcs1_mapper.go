package mapper

import (
	"assinatura-xml/config"
	"assinatura-xml/model"
	"errors"
)

type Pkcs1Mapper struct {}

func (pkcs1Mapper Pkcs1Mapper) DecodeToPkcs1(response model.ResponseInitialization) ([]model.PKCS1Dto, error) {
	data := []model.PKCS1Dto{}
	if len(response.SignedAttributes) != len(response.InitializedDocuments) {
		return data, errors.New("different number of signed attributes and initialized documents")
	}
	for index, _ := range response.SignedAttributes {
		data = append(data, model.PKCS1Dto{
			Nonce: response.SignedAttributes[index].Nonce,
			SignedAttribute: response.SignedAttributes[index].Content,
			InitializedDocument: response.InitializedDocuments[index].Content,
		})
	}

	if len(data) > 0 {
		data[0].OriginalDocument = config.DocumentPath
	}
	return data, nil
}
